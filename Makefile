install: clean sdist
	pip install dist/*.tar.gz

sdist:
	python setup.py sdist

clean:
	rm -rf dist
